CA_DIR=ca_data
CA_NAME=tokies_CA
CA_PASSWORD=qs1Z0feaTFHnY6YRHMrr
MITM_DATA=$(pwd)/mitmproxy_data
DOMAIN_NAME=host.to.intercept.com
FILE_NAME=$DOMAIN_NAME

mkdir -p $MITM_DATA
sudo cp $CA_DIR/$FILE_NAME.pem $MITM_DATA/$FILE_NAME.pem
docker run --rm -it -v $MITM_DATA:/home/mitmproxy/.mitmproxy \
                -p 8080:8080 -p 127.0.0.1:8081:8081 \
                -e SSLKEYLOGFILE='/home/mitmproxy/.mitmproxy/sslkeylogfile.txt' \
                mitmproxy/mitmproxy mitmweb \
                --web-host 0.0.0.0 \
                --certs $DOMAIN_NAME=/home/mitmproxy/.mitmproxy/$FILE_NAME.pem \
                --set ssl_version_client=all\
                --set ssl_version_server=all\
                --set termlog_verbosity=debug
