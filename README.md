## About
This repository shows how to set up a "man-in-the-middle" proxy for analyzing traffic between a device (such as e.g. a smartphone) and some external server. (Of course, this requires consent from the phone's user, i.e., that should be you!)

## Usage
* We will be using these environment variables.
  ```sh
  CA_DIR=ca_data
  CA_NAME=tokies_CA
  CA_PASSWORD=qs1Z0feaTFHnY6YRHMrr
  MITM_DATA=$(pwd)/mitmproxy_data
  DOMAIN_NAME=host.to.intercept.com
  FILE_NAME=$DOMAIN_NAME
  ```
* Create your own CA authority
  ```sh
  ### 
  # Create new CA authority
  ###
  mkdir -p $CA_DIR
  openssl genrsa -des3 -out $CA_DIR/$CA_NAME.key 4096
  openssl req -x509 -new -nodes -key $CA_DIR/$CA_NAME.key -sha256 -days 1825 -out $CA_DIR/$CA_NAME.pem
  openssl x509 -in $CA_DIR/$CA_NAME.pem -inform PEM -out $CA_DIR/$CA_NAME.crt
 
  # Example input:
  #Country Name (2 letter code) [AU]:DE
  #State or Province Name (full name) [Some-State]:BY
  #Locality Name (eg, city) []:Munich
  #Organization Name (eg, company) [Internet Widgits Pty Ltd]:QKeySoft    
  #Organizational Unit Name (eg, section) []:IT
  #Common Name (e.g. server FQDN or YOUR name) []:tokies.de
  #Email Address []:tobias@tokies.de
  ```
* Register your CA authority (`tokies_CA.crt`) on each of your devices.
  Follow e.g. the instructions listed [here](https://docs.mitmproxy.org/stable/concepts-certificates/).
  ```sh
  ###
  # Register the CA authority with Ubuntu
  ###
  sudo mkdir -p /usr/share/ca-certificates/extra
  sudo cp $CA_DIR/$CA_NAME.crt /usr/share/ca-certificates/extra/$CA_NAME.crt
  # sudo dpkg-reconfigure ca-certificates # <- Manual version
  sudo update-ca-certificates
  ```
* Sign a certificate for each domain that you want to capture on. (You can use one certificate for many domains.)
  ```sh
  openssl genrsa -out $CA_DIR/$FILE_NAME.key 4096
  openssl req -new -key $CA_DIR/$FILE_NAME.key -out $CA_DIR/$FILE_NAME.csr
  cat > $CA_DIR/$FILE_NAME.conf <<EOL
  authorityKeyIdentifier=keyid,issuer
  basicConstraints=CA:FALSE
  keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
  subjectAltName = @alt_names

  [alt_names]
  DNS.1 = $DOMAIN_NAME
  EOL
  openssl x509 -req -in $CA_DIR/$FILE_NAME.csr -CA $CA_DIR/$CA_NAME.pem -CAkey $CA_DIR/$CA_NAME.key -CAcreateserial \
          -out $CA_DIR/$FILE_NAME.crt -days 1825 -sha256 -extfile $CA_DIR/$FILE_NAME.conf
  cat $CA_DIR/$FILE_NAME.key $CA_DIR/$FILE_NAME.crt > $CA_DIR/$FILE_NAME.pem
  ```
* Start `mitmproxy` using the created certificate:
  ```sh
  mkdir -p $MITM_DATA
  sudo cp $CA_DIR/$FILE_NAME.pem $MITM_DATA/$FILE_NAME.pem
  docker run --rm -it -v $MITM_DATA:/home/mitmproxy/.mitmproxy \
                    -p 8080:8080 -p 127.0.0.1:8081:8081 \
                    -e SSLKEYLOGFILE='/home/mitmproxy/.mitmproxy/sslkeylogfile.txt' \
                    mitmproxy/mitmproxy mitmweb \
                    --web-host 0.0.0.0 \
                    --certs $DOMAIN_NAME=/home/mitmproxy/.mitmproxy/$FILE_NAME.crt \
                    --set ssl_version_client=all\
                    --set ssl_version_server=all\
                    --set termlog_verbosity=debug
  ```
                    <!-- --set ssl_insecure\ -->
                    <!-- --set ciphers_client=TLS_AES_256_GCM_SHA384\
                    --set ciphers_server=TLS_AES_256_GCM_SHA384\ -->
* As a quick test, in order to check if the setup is running somewhat reasonably well, you may run
  ```sh
  #PROXY_NAME=proxy.tokies.de
  PROXY_NAME=localhost
  export HTTP_PROXY="http://$PROXY_NAME:8080"
  export http_proxy=$HTTP_PROXY
  export HTTPS_PROXY="http://$PROXY_NAME:8080"
  ```
  and then
  ```sh
  curl --verbose -x $HTTP_PROXY  "http://$DOMAIN_NAME"
  ```
  and
  ```sh
  curl --verbose "https://$DOMAIN_NAME"
  ```
  Also helpful:
  ```
  openssl s_client -proxy=127.0.0.1:8080 -connect "$DOMAIN_NAME:443" -showcerts
  ```
  Make sure that the `$CA_DIR/$CA_NAME.crt` file is trusted by your device.
* See also the [MITMProxy Website](https://docs.mitmproxy.org/) for further configuration options.


<!-- Just do the following (where you can):
```sh
docker run --rm -it -v $(pwd)/mitmproxy_data:/home/mitmproxy/.mitmproxy \
                    -p 8080:8080 -p 127.0.0.1:8081:8081 \
                    -e SSLKEYLOGFILE='/home/mitmproxy/.mitmproxy/sslkeylogfile.txt' \
                    mitmproxy/mitmproxy mitmweb --web-host 0.0.0.0 \
                    --certs /home/mitmproxy/.mitmproxy/mitmproxy-ca.pem
```
Check the instructions [here](https://docs.mitmproxy.org/stable/concepts-modes/) or [here](https://docs.mitmproxy.org/stable/concepts-certificates/) for how to install the certificate.
Ideally, you will just have to visit (http://mitm.it) and follow the instructions there.

As a quick test, in order to check if the setup is running somewhat reasonably well, you may run
```sh
SERVER_NAME=proxy.tokies.de
export HTTP_PROXY="http://$SERVER_NAME:8080"
export HTTPS_PROXY="http://$SERVER_NAME:8080"
```
and then
```sh
curl --verbose -x $HTTP_PROXY  http://www.example.org
```
and
```sh
curl --verbose https://www.example.org
```
Generally, the [MITMProxy Website](https://docs.mitmproxy.org/) is quite useful.

*Remark:* The comments in this readme contain information on how to get a letsencrypt certificate. But this is not actually necessary for running mitmproxy. -->
<!-- 

* Specify your SERVER_NAME in the files `init-letsencrypt.sh`, `app.conf` and in the little shell command listings below.
* Run `./init-letsecnrypt.sh`. (Note: This step and the following one are not really necessary because you'll have to make the certificate trusted by the client anyway.)
* Run (and before that, create directories as necessary)
  ```sh
  # Put your own configuration here!
  SERVER_NAME=proxy.tokies.de
  MITM_DATA=mitmproxy_data
  CERT_DATA=data/certbot/conf/live/$SERVER_NAME
  sudo rm $MITM_DATA/mitmproxy-ca.pem
  sudo touch $MITM_DATA/mitmproxy-ca.pem
  sudo chown $(whoami) $MITM_DATA/mitmproxy-ca.pem
  sudo cat $CERT_DATA/privkey.pem > $MITM_DATA/mitmproxy-ca.pem
  sudo cat $CERT_DATA/fullchain.pem >> $MITM_DATA/mitmproxy-ca.pem
  ```
* Adapt the configuration in `docker-compose.yaml` as desired.
* Run `docker-compose up` (after ensuring that you have `docker-compose` installed and are member of the `docker` group).

As a quick test, in order to check if the setup is running somewhat reasonably well, you may run
```sh
SERVER_NAME=proxy.tokies.de
export HTTPS_PROXY="http://$SERVER_NAME:8080"
curl --verbose https://www.example.org
```

# Install & Run
If desired, download from [here](https://mitmproxy.org/downloads/).
Do *not* install from the repo; this may simply not work/cause additional installation trouble.
Alternatively, if `docker` is already installed, just run (assuming the current user is also member of the `docker` group)
```sh
docker run --rm -it -v $(pwd)/mitmproxy_data:/home/mitmproxy/.mitmproxy \
                    -p 8080:8080 -p 127.0.0.1:8081:8081 \
                    -e SSLKEYLOGFILE='/home/mitmproxy/.mitmproxy/sslkeylogfile.txt' \
                    mitmproxy/mitmproxy mitmweb --web-host 0.0.0.0 \
                    --certs /home/mitmproxy/.mitmproxy/mitmproxy-ca.pem
```
Confer also the [mitmproxy getting-started instructions](https://docs.mitmproxy.org/stable/overview-getting-started/).

# Little Reminder
For getting the SSL certificate running, it was necessary to run `./init-letsencrypt.sh` and then
```sh
SERVER_NAME=proxy.tokies.de
cd mitmproxy_data
sudo rm mitmproxy-ca.pem
sudo touch mitmproxy-ca.pem
sudo chown $(whoami) mitmproxy-ca.pem
sudo cat ../data/certbot/conf/live/$SERVER_NAME/privkey.pem > mitmproxy-ca.pem
sudo cat ../data/certbot/conf/live/$SERVER_NAME/cert.pem >> mitmproxy-ca.pem
```



# Debugging
`openssl s_client -connect localhost:8080 -showcerts`

# Set up device
* Set up the device to use the proxy. (Just google it.)
* Start wireshark. See also [this howto](https://docs.mitmproxy.org/stable/howto-wireshark-tls/)for further details. -->
